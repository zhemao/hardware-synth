import matplotlib.pyplot as plt
import math
import sys

if __name__ == '__main__':
    vals = [float(s.strip()) for s in sys.stdin]
    plt.plot(vals)
    plt.show()

